﻿using iTextSharp.text.pdf;
using PageCount.BussinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PageCount.Jobs
{
	public class CountJob
	{
		private int pagecount;

		private string filename;

		public CountJob(string filename)
		{
			this.filename = filename;
		}

		public void Read()
		{
			try
			{
				PdfReader pdfReader = new PdfReader(filename);
				pagecount = pdfReader.NumberOfPages;			
			}
			catch (IOException ex)
			{
				pagecount = -1;
				Console.WriteLine("Error : {0}", ex.Message);
			}
			
		}


		public void Execute(int fileId)
		{
			Read();
			if (pagecount > -1)
			{
				Console.WriteLine("Pages : {0}", pagecount);
				Connector.Insert(fileId, pagecount);
			}		
		}
	}
}
