﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PageCount.BussinessLogic
{
	public class FileModel
	{
		public FileModel(int id, string filePath, string file)
		{
			Id = id;
			FileName = Path.Combine(filePath, file);
		}

		public int Id { get; set; }


		public string FileName { get; set; }
	}
}
