﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace PageCount.BussinessLogic
{
	public static class Connector
	{
		public static FileModel[] GetFiles()
		{
			var models = new List<FileModel>();
			var connectionStr = ConfigurationManager.ConnectionStrings["pdfcounterDB"];
			SqlConnection conn = new SqlConnection(connectionStr.ConnectionString);
			var cmd = conn.CreateCommand();
			cmd.CommandText = "select id, filepath, [file] from dbo.Files";
			conn.Open();
			var reader = cmd.ExecuteReader();

			while (reader.Read())
			{
				models.Add(new FileModel((int)reader[0], (string)reader[1], (string)reader[2]));				
			}

			conn.Close();

			return models.ToArray();
		}


		public static void Insert(int id, int pagecount)
		{
			var connectionStr = ConfigurationManager.ConnectionStrings["pdfcounterDB"];
			SqlConnection conn = new SqlConnection(connectionStr.ConnectionString);
			var cmd = conn.CreateCommand();
			cmd.CommandText = "insert into PdfInfo (fileid, pagecount) values(@param1, @param2)";
			cmd.Parameters.AddWithValue("@param1", id);
			cmd.Parameters.AddWithValue("@param2", pagecount);

			conn.Open();
			cmd.ExecuteNonQuery();

			conn.Close();
		}
	}
}
