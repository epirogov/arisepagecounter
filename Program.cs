﻿using PageCount.BussinessLogic;
using PageCount.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PageCount
{
	class Program
	{
		static void Main(string[] args)
		{
			var files = Connector.GetFiles();
			foreach (var file in files)
			{
				Console.WriteLine(file.FileName);
				new CountJob(file.FileName).Execute(file.Id);
			}
		}
	}
}
